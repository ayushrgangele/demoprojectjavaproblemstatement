package com.example.Controller;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.example.Model.Lead;
import com.example.Response.ApiResponse;
import com.example.Service.LeadService;



@RestController
@RequestMapping("/api/leads")
public class LeadController {

    @Autowired
    private LeadService leadService;

    
    // 1st Problem statement code logic
    @PostMapping
    public ResponseEntity<ApiResponse> createLead(@Valid @RequestBody Lead lead) {
        ApiResponse response = leadService.createLead(lead);

        if (response.isSuccess()) {
            return ResponseEntity.ok(response);
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }
    }

    
    // 2nd Problem statement code logic
    @GetMapping("/byMobileNumber/{mobileNumber}")
    public ResponseEntity<ApiResponse> fetchLeadsByMobileNumber(@PathVariable String mobileNumber) {
        ApiResponse response = leadService.fetchLeadsByMobileNumber(mobileNumber);

        if (response.isSuccess()) {
            return ResponseEntity.ok(response);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
        }
    }

}
