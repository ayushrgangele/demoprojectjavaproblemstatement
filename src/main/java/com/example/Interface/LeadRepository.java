package com.example.Interface;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.Model.Lead;

	public interface LeadRepository extends JpaRepository<Lead, Long> {
		
		// 1st Problem statement code
		boolean existsByLeadId(Long leadId);
		
		// 2nd Problem statement code 
		List<Lead> findByMobileNumber(String mobileNumber);
    
}