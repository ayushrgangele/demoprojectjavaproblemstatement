
package com.example.Response;

public class ApiResponse {
    private String status;
    private Object data;
    private ErrorResponse errorResponse;

    private ApiResponse(String status, Object data, ErrorResponse errorResponse) {
        this.status = status;
        this.data = data;
        this.errorResponse = errorResponse;
    }

    public static ApiResponse success(Object data) {
        return new ApiResponse("success", data, null);
    }

    public static ApiResponse error(String code, String message) {
        ErrorResponse errorResponse = new ErrorResponse(code, message);
        return new ApiResponse("error", null, errorResponse);
    }

    public String getStatus() {
        return status;
    }

    public Object getData() {
        return data;
    }

    public ErrorResponse getErrorResponse() {
        return errorResponse;
    }
    
    public boolean isSuccess() {
        return "success".equals(status);
    }
}
