package com.example.Response;

public class ErrorResponse {
    private String code;
    private String[] messages;

    public ErrorResponse(String code, String... messages) {
        this.code = code;
        this.messages = messages;
    }

    public String getCode() {
        return code;
    }

    public String[] getMessages() {
        return messages;
    }
}
