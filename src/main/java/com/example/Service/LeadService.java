
package com.example.Service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.Interface.LeadRepository;
import com.example.Model.Lead;
import com.example.Response.ApiResponse;

@Service
public class LeadService {

    @Autowired
    private LeadRepository leadRepository;

    
    // 1st Problem statement code logic
    public ApiResponse createLead(Lead lead) {
        if (leadRepository.existsByLeadId(lead.getLeadId())) {
            return ApiResponse.error("E10010", "Lead Already Exists in the database with the lead id");
        }

        // Save the lead to the database (You may need to handle date conversion here)
        leadRepository.save(lead);

        return ApiResponse.success("Created Leads Successfully");
    }
    
    
    // 2nd Problem statement Code logic
    public ApiResponse fetchLeadsByMobileNumber(String mobileNumber) {
        List<Lead> leads = leadRepository.findByMobileNumber(mobileNumber);

        if (!leads.isEmpty()) {
            return ApiResponse.success(leads);
        } else {
            return ApiResponse.error("E10011", "No Lead found with the Mobile Number " + mobileNumber);
        }
    }
}
