package com.example.demo;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.example.Controller.LeadController;
import com.example.Model.Lead;
import com.example.Response.ApiResponse;
import com.example.Service.LeadService;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class LeadControllerTests {

    @Mock
    private LeadService leadService;

    @InjectMocks
    private LeadController leadController;
    
    
    // 1st Problem statement (JUnit Test using Mockito) code starts here
    @Test
    public void testCreateLead_Success() {
        Lead lead = new Lead();
        lead.setLeadId(5678L);

        when(leadService.createLead(any())).thenReturn(ApiResponse.success("Created Leads Successfully"));

        ResponseEntity<ApiResponse> responseEntity = leadController.createLead(lead);

        verify(leadService, times(1)).createLead(any());
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertEquals("success", responseEntity.getBody().getStatus());
    }

    @Test
    public void testCreateLead_Error() {
        Lead lead = new Lead();
        lead.setLeadId(5678L);

        when(leadService.createLead(any())).thenReturn(ApiResponse.error("E10010", "Lead Already Exists"));

        ResponseEntity<ApiResponse> responseEntity = leadController.createLead(lead);

        verify(leadService, times(1)).createLead(any());
        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertEquals("error", responseEntity.getBody().getStatus());
    }

    
    
    // 2nd Problem statement (JUnit Test using Mockito) code starts here
    @Test
    public void testFetchLeadsByMobileNumber_Success() {
        String mobileNumber = "8877887788";
        List<Lead> leads = new ArrayList<>();
        leads.add(new Lead());

        when(leadService.fetchLeadsByMobileNumber(mobileNumber)).thenReturn(ApiResponse.success(leads));

        ResponseEntity<ApiResponse> responseEntity = leadController.fetchLeadsByMobileNumber(mobileNumber);

        verify(leadService, times(1)).fetchLeadsByMobileNumber(mobileNumber);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertEquals("success", responseEntity.getBody().getStatus());
    }

    @Test
    public void testFetchLeadsByMobileNumber_NoLeadsFound() {
        String mobileNumber = "8877887788";

        when(leadService.fetchLeadsByMobileNumber(mobileNumber)).thenReturn(ApiResponse.error("E10011", "No Lead found with the Mobile Number " + mobileNumber));

        ResponseEntity<ApiResponse> responseEntity = leadController.fetchLeadsByMobileNumber(mobileNumber);

        verify(leadService, times(1)).fetchLeadsByMobileNumber(mobileNumber);
        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertEquals("error", responseEntity.getBody().getStatus());
    }

}
